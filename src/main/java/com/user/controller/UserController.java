package com.user.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import java.util.*;

import com.user.entity.Contact;
import com.user.entity.User;
import com.user.service.UserService;

@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	private UserService userService;

	@Autowired
	private RestTemplate restTemplate;

	@GetMapping("/{userId}")
	public User getUSer(@PathVariable("userId") Long userId) {

		User user = userService.getUSer(userId);

		// http://localhost:9002/contact/c/6666

		List contacts = this.restTemplate.getForObject("http://contact-service:9002/contact/c/"+user.getUserId(), List.class);
		System.out.println(contacts.toString());
		user.setContacts(contacts);
		// return userService.getUSer(userId);
		return user;

	}
	
	@GetMapping("/display")
	public String display()
	{
		return "commit - 1 new";
	}
	
	@GetMapping("/displayw")
	public String displayw()
	{
		return "commit - 3 new";
	}

}
