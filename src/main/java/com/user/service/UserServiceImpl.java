package com.user.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.user.entity.User;
@Service
public class UserServiceImpl implements UserService {

	@Override
	public User getUSer(Long id) {

		List<User> list = new ArrayList<>();
		list.add(new User(3L, "Neha", "9097", null));
		list.add(new User(1L, "Ankita", "8888",null));
		list.add(new User(2L, "Akansha", "7777",null));

		return list.stream().filter(user -> user.getUserId().equals(id)).findAny().orElse(null);
	}

	
}
